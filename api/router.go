package api

import (
	"net/http"

	"gitlab.com/test-kafka/api-gateway/config"
	"gitlab.com/test-kafka/api-gateway/pkg/logger"
	"gitlab.com/test-kafka/api-gateway/services"
	"gitlab.com/test-kafka/api-gateway/storage/repo"

	_ "gitlab.com/test-kafka/api-gateway/api/docs"
	v2 "gitlab.com/test-kafka/api-gateway/api/handlers/v2"
	"gitlab.com/test-kafka/api-gateway/api/middleware"
	"gitlab.com/test-kafka/api-gateway/api/token"

	"github.com/gin-contrib/cors"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemorystorageI
	CasbinEnforcer *casbin.Enforcer
}

// @title  Exam API
// @version 1.0
// @description This is a user service.
// @termsOfService 2 term exam

// @contact.name Azam
// @contact.url https://t.me/alievazam
// @contact.email alievazam.200@swagger.io

// @host 54.159.1.66:8000

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwtHandler := token.JWTHandler{
		SigninKey: option.Conf.SignInKey,
		Log:       option.Logger,
	}

	handlerV2 := v2.New(&v2.HandlerV2Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
		JWTHandler:     jwtHandler,
	})

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))
	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "App is running",
		})
	})
	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwtHandler, config.Load()))
	api := router.Group("/v2")
	// CustomerService
	api.POST("/customer", handlerV2.CreateCustomer)
	api.GET("/customer/:id", handlerV2.GetCustomerByID)
	api.GET("/customers/:id", handlerV2.GetCustomerAllinfo)
	api.DELETE("/delete/:id", handlerV2.DeleteInfo)
	api.PATCH("/update/:id", handlerV2.UpdateByID)
	api.GET("/customerlist/:page/:limit", handlerV2.GetLists)

	//  Postservice
	api.POST("/post", handlerV2.CreatePost)
	api.GET("/post/:id", handlerV2.GetPostById)
	// api.GET("/posts/:id", handlerV2.GetByCustId)
	api.PATCH("/post/update/:id", handlerV2.UpdatePost)
	api.DELETE("/post/delete/:id", handlerV2.DeleteDatabase)
	api.GET("/posts/search/order", handlerV2.GetPostBySearhAndOrder)

	// Reviewservice
	api.POST("/review", handlerV2.CreateReview)
	api.GET("/review/:id", handlerV2.GetReviewById)
	api.GET("/reviews/:id", handlerV2.GetReviewByPostId)
	api.PATCH("/review/update/:id", handlerV2.UpdateReview)
	api.DELETE("/review/deleted/:id", handlerV2.DeleteByCustomerId)
	api.DELETE("/review/delete/:id", handlerV2.DeleteByPostId)

	// Register
	api.POST("/register", handlerV2.Register)
	api.GET("/verify/:email/:code", handlerV2.Verifacition)
	api.GET("/login/:email/:password", handlerV2.Login)

	// Login
	api.GET("/login/admin/:username/:password", handlerV2.LoginAdmin)
	api.GET("/login/moderator/:username/:password", handlerV2.LoginModerator)

	// Swagger
	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler, url))

	return router

}
