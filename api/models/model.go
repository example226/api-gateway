package models

type CreateCustomer struct {
	FirstName   string
	LastName    string
	Email       string
	Username    string
	Password    string
	Bio         string
	PhoneNumber string
	Addresses   []*Address
}

type Customer struct {
	FirstName    string
	LastName     string
	Email        string
	Username     string
	Password     string
	Bio          string
	PhoneNumber  string
	Addresses    []*Address
	Code         string
	Refreshtoken string
}

type CustomerUpdate struct {
	FirstName   string
	LastName    string
	Bio         string
	PhoneNumber string
	Email       string
	Username    string
	Password    string
	Addresses   []*Address
}
type Address struct {
	District string
	Street   string
}

type User struct {
	FirstName string
	LastName  string
	Email     string
	Username  string
	Uuid      string
}

type Verify struct {
	Id           string
	FirstName    string
	LastName     string
	Email        string
	Username     string
	AccessToken  string
	RefreshToken string
}

type Login struct {
	Email         string
	FirstName     string
	LastName      string
	Password      string
	Username      string
	Refreshtoken  string
	Accessestoken string
	Uuid          string
}

type Admin struct {
	Email         string
	Password      string
	Username      string
	FirstName     string
	LastName      string
	AccessesToken string
}

type UpdatePost struct {
	Name        string
	Description string
	Medias      []*Media
}

type Post struct {
	Name        string
	Description string
	Medias      []*Media
}

type Media struct {
	Link string
	Name string
	Type string
}

type Review struct {
	Description string
	Name        string
	Rating      int64
	Post_id     int64
}
