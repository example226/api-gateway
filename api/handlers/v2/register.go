package v2

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/customer"
	"gitlab.com/test-kafka/api-gateway/pkg/etc"
	l "gitlab.com/test-kafka/api-gateway/pkg/logger"
	"gitlab.com/test-kafka/api-gateway/pkg/utils"

	"golang.org/x/crypto/bcrypt"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

// Register
// @Summary      Register
// @Description  Registration
// @Tags         Register
// @Accept       json
// @Produce      json
// @Param        customer   body models.Customer     true  "Customers"
// @Success      200  {object}  models.Customer
// @Router       /v2/register [post]
func (h *handlerV2) Register(c *gin.Context) {
	var (
		body models.Customer
	)
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while bind json", l.Error(err))
		return
	}

	err = utils.IsValidMail(body.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid email address",
		})
		return
	}
	fmt.Println(body)

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	ctx, cancel := context.WithTimeout(context.Background(),
		time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	existsEmail, err := h.serviceManager.CustomerService().CheckField(ctx, &customer.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed check email uniques", l.Error(err))
		return
	}
	if existsEmail.Exists {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "please enter another email",
		})
		h.log.Error("this email already exists", l.Error(err))
		return
	}

	exists, err := h.redis.Exists(body.Email)
	fmt.Println(exists)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
			"info":  "please enter another email",
		})
		h.log.Error("failed check email uniques", l.Error(err))
		return
	}

	if cast.ToInt(exists) == 1 {
		c.JSON(http.StatusConflict, gin.H{
			"error": err.Error(),
		})
		return
	}

	hashPass, err := bcrypt.GenerateFromPassword([]byte(body.Password), 14)

	if err != nil {
		h.log.Error("error while hashing password", l.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong",
		})
		return
	}
	code := etc.GenerateCode(6)
	body.Password = string(hashPass)
	add := []*models.Address{}
	for _, i := range body.Addresses {
		a := models.Address{
			District: i.District,
			Street:   i.Street,
		}
		add = append(add, &a)
	}
	ref := &models.Customer{
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Email:       body.Email,
		Username:    body.Username,
		Bio:         body.Bio,
		PhoneNumber: body.PhoneNumber,
		Password:    body.Password,
		Code:        code,
		Addresses:   add,
	}
	msg := "Subject: Exam email verification\n Your verification code: " + ref.Code
	err = utils.SendMail([]string{ref.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
			"info":  "Your Email is not valid, Please recheck it",
		})
		return
	}

	jsCustomer, err := json.Marshal(ref)

	if err != nil {
		h.log.Error("error while marshaling customer,inorder to insert it to redis", l.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while creating customer",
		})
		return
	}

	if err = h.redis.SetWithTTL(string(ref.Email), string(jsCustomer), 86000); err != nil {
		fmt.Println(err)
		h.log.Error("error while inserting new customer into redis")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
		return
	}
}
