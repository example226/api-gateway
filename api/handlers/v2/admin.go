package v2

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/test-kafka/api-gateway/genproto/customer"
	"gitlab.com/test-kafka/api-gateway/genproto/post"
	"gitlab.com/test-kafka/api-gateway/genproto/review"
	l "gitlab.com/test-kafka/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// DeleteDatabase
// @Summary      DeleteCustomer
// @Description  Delete Customer info by Id
// @Tags         Admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path string    true  "customer_id"
// @Success      200
// @Router       /v2/delete/{id} [delete]
func (h *handlerV2) DeleteInfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().DeleteCustomer(
		ctx, &customer.ID{
			Uuid: ids,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)
}

// UpdatePost
// @Summary      UpdatePost
// @Description  Update Post
// @Tags         Moderator
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        post body post.Post    true "Update"
// @Success      200  {object}  post.Post
// @Router       /v2/post/update [patch]
func (h *handlerV2) UpdatePostModerator(c *gin.Context) {

	var (
		jspbMarshal protojson.MarshalOptions
		body        post.Post
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.PostService().UpdatePost(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update customer")
		return
	}
	c.JSON(http.StatusOK, response)

}

// DeleteByPostid
// @Summary      DeleteByPostId
// @Description  Delete Review info by CustomerId
// @Tags        Admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200
// @Router       /v2/review/delete/{id} [delete]
func (h *handlerV2) DeleteByPostId(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(id)
	response, err := h.serviceManager.ReviewService().DeleteByPostId(
		ctx, &review.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete by postId review")
		return
	}
	c.JSON(http.StatusOK, response)
}
