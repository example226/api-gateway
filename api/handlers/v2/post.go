package v2

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/post"
	l "gitlab.com/test-kafka/api-gateway/pkg/logger"
	"gitlab.com/test-kafka/api-gateway/pkg/utils"
)

// Create Post
// @Summary      Create post
// @Description  Creates new post
// @Tags         Post
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        post   body models.Post     true  "Post"
// @Success      200  {object}  post.PostResp
// @Router       /v2/post [post]
func (h *handlerV2) CreatePost(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}

	var (
		body        models.Post
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	media := []*post.MediasReq{}
	for _, i := range body.Medias {
		meds := post.MediasReq{
			Name: i.Name,
			Link: i.Link,
			Type: i.Type,
		}
		media = append(media, &meds)
	}

	response, err := h.serviceManager.PostService().CreatePost(ctx, &post.PostReq{
		Name:        body.Name,
		Description: body.Description,
		Uuid:        claims.Sub,
		Medias:      media,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create store", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// GetPostById
// @Summary      GetPostById
// @Description  Get Post info by Id
// @Tags         Post
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "post_id"
// @Success      200  {object}  post.GetPostInfo
// @Router       /v2/post/{id} [get]
func (h *handlerV2) GetPostById(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.PostService().GetPostById(
		ctx, &post.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get post")
		return
	}
	c.JSON(http.StatusOK, response)

}

// // GetPostById
// // @Summary      GetCustById
// // @Description  Get Post info by custom Id
// // @Tags         Post
// // @Accept       json
// // @Produce      json
// // @Param        id path int    true  "customer_id"
// // @Success      200  {object}  post.GetPosts
// // @Router       /v2/posts/{id} [get]
// func (h *handlerV2) GetByCustId(c *gin.Context) {
// 	var jspbMarshal protojson.MarshalOptions
// 	jspbMarshal.UseProtoNames = true

// 	ids := c.Param("id")
// 	id, err := strconv.ParseInt(ids, 10, 64)
// 	fmt.Println(id)
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("failed parse string to int", l.Error(err))
// 		return
// 	}
// 	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
// 	defer cancel()
// 	res := &post.ID{Id: id}
// 	fmt.Println(res)
// 	response, err := h.serviceManager.PostService().GetByCustId(
// 		ctx, res)
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("failed to get post")
// 		return
// 	}
// 	c.JSON(http.StatusOK, response)

// }

// UpdatePost
// @Summary      UpdatePost
// @Description  Update Post
// @Tags         Post
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param		id path int	true "id"
// @Param        post body models.UpdatePost    true "Update"
// @Success      200  {object}  post.Post
// @Router       /v2/post/update/{id} [patch]
func (h *handlerV2) UpdatePost(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}

	var (
		jspbMarshal protojson.MarshalOptions
		body        models.UpdatePost
	)
	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	jspbMarshal.UseProtoNames = true
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse body", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	medias := []*post.MediasResp{}
	for _, i := range body.Medias {
		media := &post.MediasResp{
			PostId: id,
			Name:   i.Name,
			Link:   i.Link,
			Type:   i.Type,
		}
		medias = append(medias, media)
	}
	if claims.Role == "moderator" || claims.Role == "admin" {
		res, err := h.serviceManager.PostService().UpdatePost(ctx, &post.Post{
			PostId:      id,
			Name:        body.Name,
			Description: body.Description,
			Medias:      medias,
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			h.log.Error("failed to update customer")
			return
		}
		c.JSON(http.StatusOK, res)
		fmt.Println(res)
	} else {
		response, err := h.serviceManager.PostService().UpdatePost(ctx, &post.Post{
			PostId:      id,
			Name:        body.Name,
			Description: body.Description,
			Medias:      medias,
			Uuid:        claims.Sub,
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			h.log.Error("failed to update customer")
			return
		}
		c.JSON(http.StatusOK, response)
	}

}

// DeleteDatabase
// @Summary      DeletePostInfoById
// @Description  Delete Post info by Id
// @Tags        Admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int   true  "post_id"
// @Success      200
// @Router       /v2/post/delete/{id} [delete]
func (h *handlerV2) DeleteDatabase(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.PostService().DeleteDatabase(
		ctx, &post.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)
}

// DeleteByCustId
// @Summary      DeleteDatabase
// @Description  Delete Post by Customer Id
// @Tags        Admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int   true  "customer_id"
// @Success      200
// @Router       /v2/post/delet/{id} [delete]
func (h *handlerV2) DeleteData(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.PostService().DeleteDatabase(
		ctx, &post.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get Post list by search and order by
// @Summary 		Get posts by search and order
// @Description 	Get Post list by search and order by:
// @Description     Search fields -> title, description, content
// @Description     ORDER fields -> field:DESC or field:ASC
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           page        query     int	    true "Page"
// @Param           limit       query     int	    true "limit"
// @Param           search      query     string    true "Search format should be 'key:value'"
// @Param           order       query     string    true "order format should be 'key:value'"
// @Success         200					  {object} 	post.GetPosts
// @Router          /v2/posts/search/order [get]
func (h *handlerV2) GetPostBySearhAndOrder(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	search := strings.Split(c.Query("search"), ":")
	order := strings.Split(c.Query("order"), ":")
	if len(search) != 2 && len(order) != 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"info": "error",
		})
		h.log.Error("failed to get all params")
		return
	}
	params, errStr := utils.ParseQueryParams(queryParams)
	if errStr != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"info": "error",
		})
		h.log.Error("failed to parse query params" + errStr[0])
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.PostService().GetListPostBySearch(ctx, &post.GetListBySearch{
		Page:  params.Page,
		Limit: params.Limit,
		Search: &post.SearchFields{
			Field:  search[0],
			Values: search[1],
		},
		Orders: &post.Order{
			Field:  order[0],
			Values: order[1],
		}})
	if errStr != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "couldn't Get",
		})
		h.log.Error("Get .Post().GetPostBySearchAndOrder(ctx, &ps.GetListBySearchReq{", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, res)
}
