package v2

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/customer"
	l "gitlab.com/test-kafka/api-gateway/pkg/logger"

	"github.com/google/uuid"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

// Verify customer
// @Summary      Verify customer
// @Description  Verifys customer
// @Tags         Register
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  models.Verify
// @Router      /v2/verify/{email}/{code} [get]
func (h *handlerV2) Verifacition(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)

	sredis, err := h.redis.Get(email)
	fmt.Println(email, code)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting user from redis", l.Any("redis", err))
		return
	}
	cs := cast.ToString(sredis)
	// body := customer.CustomerReq{}
	csStr := models.Customer{}
	err = json.Unmarshal([]byte(cs), &csStr)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to user csStr", l.Any("json", err))
		return
	}

	if csStr.Code != code {
		fmt.Println(csStr.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}

	id := uuid.New()

	body := customer.CustomerReq{
		FirstName:    csStr.FirstName,
		LastName:     csStr.LastName,
		Email:        csStr.Email,
		Username:     csStr.Username,
		Bio:          csStr.Bio,
		PhoneNumber:  csStr.PhoneNumber,
		Password:     csStr.Password,
		Refreshtoken: csStr.Refreshtoken,
		Uuid:         id.String(),
	}
	h.jwthandler.Iss = "user"
	h.jwthandler.Sub = body.Uuid
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}

	body.Refreshtoken = refreshToken
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	res, err := h.serviceManager.CustomerService().CreateCustomer(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", l.Any("post", err))
		return
	}

	response := models.Verify{
		Id:        res.Uuid,
		FirstName: res.FirstName,
		LastName:  res.LastName,
		Email:     res.Email,
		Username:  res.Username,
	}

	response.AccessToken = accessToken
	response.RefreshToken = refreshToken

	c.JSON(http.StatusOK, response)

}
