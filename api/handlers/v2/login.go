package v2

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/customer"
	"gitlab.com/test-kafka/api-gateway/pkg/etc"
	"gitlab.com/test-kafka/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Login user
// @Summary      Login user
// @Description  Logins user
// @Tags         Customer
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        password   path string true "password"
// @Success         200                   {object}  models.Login
// Failure         500                   {object}  models.Error
// Failure         400                   {object}  models.Error
// Failure         404                   {object}  models.Error
// @Router      /v2/login/{email}/{password} [get]
func (h *handlerV2) Login(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		password = c.Param("password")
		email    = c.Param("email")
	)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().GetByEmail(ctx, &customer.LoginReq{
		Email: email,
	})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Couln't find matching information, Have you registered before?",
		})
		h.log.Error("Error while getting customer by email", logger.Any("post", err))
		return
	}

	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Password or Email error",
		})
		return
	}

	h.jwthandler.Iss = "user"
	h.jwthandler.Sub = res.CustomerUuid
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessesToken := tokens[0]
	refreshToken := tokens[1]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	response := &models.Login{
		Email:     res.Email,
		FirstName: res.FirstName,
		LastName:  res.LastName,
		Password:  res.Password,
		Username:  res.Username,
		Uuid:      res.CustomerUuid,
	}

	response.Refreshtoken = refreshToken
	response.Accessestoken = accessesToken
	response.Password = ""
	c.JSON(http.StatusOK, response)
}
