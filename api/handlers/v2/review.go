package v2

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/review"
	l "gitlab.com/test-kafka/api-gateway/pkg/logger"

	// "strconv"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create Review
// @Summary      Create Review
// @Description  Creates new review
// @Tags         Review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        review   body models.Review     true  "User"
// @Success      200  {object}  review.ReviewResp
// @Router       /v2/review [post]
func (h *handlerV2) CreateReview(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}

	var (
		body        models.Review
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ReviewService().CreateReview(ctx, &review.ReviewReq{
		PostId:       body.Post_id,
		CustomerUuid: claims.Sub,
		Rating:       body.Rating,
		Name:         body.Name,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create product", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// GetReviewById
// @Summary      GetReviewById
// @Description  Get Review info by Id
// @Tags         Review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200  {object}  review.Review
// @Router       /v2/review/{id} [get]
func (h *handlerV2) GetReviewById(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().GetReviewById(ctx, &review.ID{
		Id: id,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get review")
		return
	}
	c.JSON(http.StatusOK, response)

}

// GetReviewByPostId
// @Summary      GetReviewByPostId
// @Description  Get Review info by Post Id
// @Tags         Review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200  {object}  review.GetRewiewsRes
// @Router       /v2/reviews/{id} [get]
func (h *handlerV2) GetReviewByPostId(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	fmt.Println(ids)
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().GetByPostId(ctx, &review.ID{
		Id: id,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get review")
		return
	}
	c.JSON(http.StatusOK, response)

}

// UpdateReview
// @Summary      Updatereview
// @Description  Update Review
// @Tags         Review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param		id path int	true "id"
// @Param        post body models.Review    true "Update"
// @Success      200  {object}  review.Review
// @Router       /v2/review/update/{id} [patch]
func (h *handlerV2) UpdateReview(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}
	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to Parse", l.Error(err))
		return
	}
	var (
		jspbMarshal protojson.MarshalOptions
		body        models.Review
	)
	jspbMarshal.UseProtoNames = true
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	if claims.Role == "moderator" || claims.Role == "admin" {
		response, err := h.serviceManager.ReviewService().UpdateReview(ctx, &review.Review{
			Id:     id,
			PostId: body.Post_id,
			Rating: body.Rating,
			Name:   body.Name,
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			h.log.Error("failed to update review")
			return
		}
		c.JSON(http.StatusOK, response)
	} else {
		response, err := h.serviceManager.ReviewService().UpdateReview(ctx, &review.Review{
			Id:           id,
			PostId:       body.Post_id,
			Rating:       body.Rating,
			Name:         body.Name,
			CustomerUuid: claims.Sub,
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			h.log.Error("failed to update review")
			return
		}
		c.JSON(http.StatusOK, response)
	}
}

// DeleteByCustomerId
// @Summary      DeleteByCustomerId
// @Description  Delete Review info by CustomerId
// @Tags        Admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path string    true  "id"
// @Success      200
// @Router       /v2/review/deleted/{id} [delete]
func (h *handlerV2) DeleteByCustomerId(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")

	if ids != claims.Sub || claims.Role != "moderator" && claims.Role != "admin" {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Don't delete it because it's not your review",
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().DeleteByCustomerId(
		ctx, &review.Ids{
			Uuid: ids,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete review")
		return
	}
	c.JSON(http.StatusOK, response)
}
