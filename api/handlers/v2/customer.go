package v2

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/customer"
	l "gitlab.com/test-kafka/api-gateway/pkg/logger"
	"gitlab.com/test-kafka/api-gateway/pkg/utils"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create Customer
// @Summary      Create customer
// @Description  Creates new customer
// @Tags         Customer
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        customer   body models.CreateCustomer     true  "Customers"
// @Success      200  {object}  customer.CustomerResp
// @Router       /v2/customer [post]
func (h *handlerV2) CreateCustomer(c *gin.Context) {
	var (
		body        models.CreateCustomer
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	id := uuid.New()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(body)
	ad := []*customer.AddressReq{}
	for _, i := range body.Addresses {
		a := &customer.AddressReq{
			District: i.District,
			Street:   i.Street,
		}
		ad = append(ad, a)
	}
	tokens, err := h.jwthandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while generate tokens",
		})
		h.log.Error("failed to give token", l.Error(err))
		return
	}
	// accessToken := tokens[0]
	refreshToken := tokens[1]
	response, err := h.serviceManager.CustomerService().CreateCustomer(ctx, &customer.CustomerReq{
		Uuid:         id.String(),
		FirstName:    body.FirstName,
		LastName:     body.LastName,
		Bio:          body.Bio,
		Addresses:    ad,
		Email:        body.Email,
		Password:     body.Password,
		Username:     body.Username,
		PhoneNumber:  body.PhoneNumber,
		Refreshtoken: refreshToken,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create customer", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// GetCustomerById
// @Summary      GetCustomerById
// @Description  Get Customer  By Id
// @Tags         Customer
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path string    true  "customer_id"
// @Success      200  {object}  customer.Customer
// @Router       /v2/customer/{id} [get]
func (h *handlerV2) GetCustomerByID(c *gin.Context) {
	ids := c.Param("id")

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.CustomerService().GetCustomerById(
		ctx, &customer.ID{Uuid: ids})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)

}

// GetCustomerAllinfo
// @Summary      GetCustomerById
// @Description  Get Customer  By Id
// @Tags         Customer
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path string    true  "customer_id"
// @Success      200  {object}  customer.GetCustomer
// @Router       /v2/customers/{id} [get]
func (h *handlerV2) GetCustomerAllinfo(c *gin.Context) {
	ids := c.Param("id")

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().GetCustomerAllinfo(
		ctx, &customer.ID{
			Uuid: ids,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)

}

// UpdateCustomer
// @Summary      UpdateCustomer
// @Description  Update Customer
// @Tags         Admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param		id path string true "uuid"
// @Param        customer body models.CustomerUpdate    true "Update"
// @Success      200  {object}  models.CustomerUpdate
// @Router       /v2/update/{id} [patch]
func (h *handlerV2) UpdateByID(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed get claims", l.Error(err))
		return
	}
	var (
		jspbMarshal protojson.MarshalOptions
		body        models.CustomerUpdate
	)
	ids := c.Param("id")
	jspbMarshal.UseProtoNames = true
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int64", l.Error(err))
		return
	}
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}

	if claims.Sub != ids && claims.Role != "moderator" || claims.Sub !=ids && claims.Role != "admin" {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Don't update it because it's not your",
		})
		h.log.Error("failed update customer", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	addr := []*customer.Address{}
	for _, i := range body.Addresses {
		ad := &customer.Address{
			Uuid:     claims.Sub,
			District: i.District,
			Street:   i.Street,
		}
		addr = append(addr, ad)
	}

	response, err := h.serviceManager.CustomerService().UpdateCustomer(ctx, &customer.Customer{
		Uuid:        ids,
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Bio:         body.Bio,
		PhoneNumber: body.PhoneNumber,
		Email:       body.Email,
		Addresses:   addr,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update customer")
		return
	}
	c.JSON(http.StatusOK, response)

}

// @Summary list customer
// @Description lists customers
// @Tags Customer
// @Security     BearerAuth
// @Accept json
// @Produce json
// @Param page query string false "query params"
// @Param limit query string false "query params"
// @Success 200 {object} customer.CustomerResp
// @Router /v2/customerlist/{page}/{limit} [get]
func (h *handlerV2) GetLists(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	params, errString := utils.ParseQueryParams(queryParams)
	if errString != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": errString,
		})
		h.log.Error("failed pars queryparams")
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(params.Page, params.Limit)
	response, err := h.serviceManager.CustomerService().GetLists(ctx, &customer.ListReq{
		Page:  params.Page,
		Limit: params.Limit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed insert to GetLists")
		return
	}
	c.JSON(http.StatusAccepted, response)

}
