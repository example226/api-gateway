package v2

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/test-kafka/api-gateway/api/models"
	"gitlab.com/test-kafka/api-gateway/genproto/customer"
	"gitlab.com/test-kafka/api-gateway/pkg/etc"
	"gitlab.com/test-kafka/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Login admin
// @Summary      Login admin
// @Description  Logins admin
// @Tags         Admin
// @Accept       json
// @Produce      json
// @Param        username path string true "username"
// @Param        password   path string true "password"
// @Success         200                   {object}  models.Admin
// Failure         500                   {object}  models.Error
// Failure         400                   {object}  models.Error
// Failure         404                   {object}  models.Error
// @Router      /v2/login/admin/{username}/{password} [get]
func (h *handlerV2) LoginAdmin(c *gin.Context) {
	// fmt.Println(c.Param("username"))
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		password = c.Param("password")
		username = c.Param("username")
	)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().GetAdminByUsername(ctx, &customer.AdminReq{
		Username: username,
	})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Couln't find matching information, Have you registered before?",
		})
		h.log.Error("Error while getting customer by email", logger.Any("post", err))
		return
	}

	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Password or Email wrong",
		})
		return
	}

	h.jwthandler.Iss = "admin"
	h.jwthandler.Sub = fmt.Sprint(res.Id)
	h.jwthandler.Role = "admin"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessesToken := tokens[0]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	response := &models.Admin{
		Password: res.Password,
		Username: res.Username,
	}

	response.AccessesToken = accessesToken
	response.Password = ""
	c.JSON(http.StatusOK, response)
}
