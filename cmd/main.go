package main

import (
	"gitlab.com/test-kafka/api-gateway/api"
	"gitlab.com/test-kafka/api-gateway/config"
	"gitlab.com/test-kafka/api-gateway/pkg/logger"
	"gitlab.com/test-kafka/api-gateway/services"
	r "gitlab.com/test-kafka/api-gateway/storage/redis"

	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"

	fileadapter "github.com/casbin/casbin/v2/persist/file-adapter"
	"github.com/gomodule/redigo/redis"
)

func main() {
	var (
		casbinEnforcer *casbin.Enforcer
	)
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "api_gateway")

	// psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
	// 	cfg.PostgresHost,
	// 	cfg.PostgresPort,
	// 	cfg.PostgresUser,
	// 	cfg.PostgresPassword,
	// 	cfg.PostgresDB,
	// )
	// enf, err := gormadapter.NewAdapter("postgres", psqlString, true)
	// if err != nil {
	// 	log.Error("while connect to newAdapter", logger.Error(err))
	// 	return
	// }
	rules := fileadapter.NewAdapter("./config/casbin_rules.csv")
	casbinEnforcer, err := casbin.NewEnforcer(cfg.AuthConfigPath, rules)
	if err != nil {
		log.Error("casbin enforcer error", logger.Error(err))
		return
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error("error while load policy")
		return
	}

	serviceManager, err := services.NewServiceManager(&cfg)
	if err != nil {
		log.Error("gRPC dial error", logger.Error(err))
		return
	}

	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch3", util.KeyMatch3)

	rdb := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", cfg.RedisHost+":"+cfg.RedisPort)
		},
	}

	server := api.New(api.Option{
		Conf:           cfg,
		Logger:         log,
		ServiceManager: serviceManager,
		Redis:          r.NewRedisRepo(rdb),
		CasbinEnforcer: casbinEnforcer,
	})

	if err := server.Run(cfg.HTTPPort); err != nil {
		log.Fatal("failed to run http server",
			logger.Error(err))
		panic(err)
	}
}
